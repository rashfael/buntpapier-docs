import Vue from 'vue'
import Buntpapier from 'buntpapier'

import App from './App'

Vue.use(Buntpapier)

new Vue(App)
